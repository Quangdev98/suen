

$(document).ready(function () {


	$(".icon-bar").click(function () {
		$("header#header").toggleClass("active");
		$(".menu").toggleClass("active");
		$("body").toggleClass('fixed');
	});


	// function setWidthText() {
	// 	let showChar = 350;
	// 	var ellipsestext = "...";
	// 	var data = ['box-content-ykien p'];
	// 	data.forEach(function (value) {

	// 		$('.' + value).each(function () {
	// 			var content = $(this).html();
	// 			if (content.length > showChar) {
	// 				var c = content.substr(0, showChar);
	// 				var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp';
	// 				$(this).html(html);
	// 			}

	// 		});
	// 	});
	// }
	// setWidthText();

	$(".title-toggle-copdev").click(function () {
		$(this).toggleClass('active');
		$(this).siblings('.content-copdev-item').slideToggle();
	});

	$(".icon-search-header").click(function () {
		$(this).parents('.wrap-search').toggleClass("active");
		$("body").toggleClass('fixed');
	});

	$(".icon-toggleSubmenu").click(function () {
		$(this).toggleClass('active')
		$(this).siblings('.wrap-memu-child').slideToggle();
		$("body").addClass('fixed');
	});

	$(document).mouseup(function (e) {
		var container = $(".form-search-header");
		if (!container.is(e.target) &&
			container.has(e.target).length === 0) {
			$('.wrap-search').removeClass('active');
			$("body").removeClass('fixed');
		}

		var container = $(".menu.active nav#nav ul, header");
		if (!container.is(e.target) &&
			container.has(e.target).length === 0) {
			$('section.menu').removeClass('active');
			$("body").removeClass('fixed');
			$("header#header").removeClass("active");
		}
	});

	var widthDiveces = $(window).width();
	$(window).on('scroll', function () {
		if(widthDiveces <= 1024){
			if ($(window).scrollTop()) {
				$('header#header').addClass('scroll');
			} else {
				$('header#header').removeClass('scroll')
			};
		}
	});


	var count = 1;
    $(".minus").click(function(){
        if (count > 1) {
        count--;
        $("#number_gues").val(+count);
    }
    });
    $(".plus").click(function(){
        if (count <1000) {
          count++;
           $("#number_gues").val(+count);
        }
    });



});

$('#slide-main').owlCarousel({
	loop: true,
	nav: true,
	autoplay: true,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	responsive: {
		0: {
			items: 1
		},
		540: {
			items: 1
		},
		768: {
			items: 1
		},
		1024: {
			items: 1
		}
	}
});
$('#slide-story').owlCarousel({
	loop: true,
	nav: true,
	autoplay: true,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	responsive: {
		0: {
			items: 2,
			margin:20
		},
		991: {
			items: 1
		}
	}
});
// slide-product-hot
$('#slide-product-hot').owlCarousel({
	loop: true,
	nav: true,
	autoplay: true,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	responsive: {
		0: {
			items: 1,
			margin: 10
		},
		375: {
			items: 2,
			margin: 20
		},
		768: {
			items: 3,
			margin: 30
		},
		1024: {
			items: 4,
			margin: 60
		}
	}
});
// proud
$('#slide-proud').owlCarousel({
	loop: true,
	nav: true,
	autoplay: true,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	margin: 10,
	responsive: {
		0: {
			items: 1
		},
		540: {
			items: 2
		}
	}
});

// blog
$('#slide-blog').owlCarousel({
	loop: true,
	nav: true,
	autoplay: true,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	margin: 30,
	responsive: {
		0: {
			items: 1
		},
		540: {
			items: 2
		},
		991: {
			items: 3,
		}
	}
});

$('#qqqr').owlCarousel({
    loop: true,
	nav: true,
	autoplay: false,
	margin: 10,
    responsive:{
        0:{
            items:2
        },
		375:{
            items:3
        },
        576:{
            items:4
        }
    }
});


